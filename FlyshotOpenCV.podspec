#
#  Be sure to run `pod spec lint FlyshotOpenCV.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "FlyshotOpenCV"
  spec.version      = "1.0.0"
  spec.summary      = "Flyshot OpenCV SDK"
  spec.homepage     = "https://bitbucket.org/flyshot/opencv-ios"

  spec.license      = { :type => "Flyshot Platform License", :file => "LICENSE" }
  spec.author       = "Flyshot"

  spec.platform     = :ios, "10.0"
  spec.source       = { :git => "https://bitbucket.org/flyshot/opencv-ios.git", :tag => "1.0.0" }

  spec.library    = "stdc++"
  spec.framework = "CoreMedia", "AVFoundation"
  spec.vendored_frameworks = "opencv2.framework"

end
